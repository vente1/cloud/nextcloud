FROM nextcloud:apache

RUN apt-get update && \
	apt-get install -y --no-install-recommends libsmbclient-dev=2:4.9.5+dfsg-5+deb10u1 && \
	rm -rf /var/lib/apt/lists/* && \
	pecl install smbclient && \
	echo "extension=smbclient.so" > /usr/local/etc/php/conf.d/docker-php-ext-smbclient.ini
